const CategoryDataAccess = require('../dataAccess/category');
const RecordController = require('./record');

module.exports.createCategory = async (categoryObj) => {
	const category = await CategoryDataAccess
		.getCategory({ name: categoryObj.name, user: categoryObj.user});

	if (category !== null) {
		if (category.budgetType !== categoryObj.budgetType) {
			return { fail: 'category-exists-as-another-type'}
		}
		return { fail: 'category-exists'}
	}

	const res = await CategoryDataAccess.createCategory(categoryObj);
	if (res === true) {
		return { success: 'success' }
	} else {
		return { fail: res }
	}
}

module.exports.getAllCategories = async (userId) => {
	const query = { user: userId};
	const categories = await CategoryDataAccess.getCategories(query);
	return { success: categories };
}

module.exports.getAllIncomeCategories = async (userId) => {
	const query = { user: userId, budgetType: 'income'}
	const categories = await CategoryDataAccess.getCategories(query);
	return { success: categories };
}

module.exports.getAllExpenseCategories = async (userId) => {
	const query = { user: userId, budgetType: 'expense'}
	const categories = await CategoryDataAccess.getCategories(query);
	return { success: categories };
}

module.exports.updateCategory = async({ userId, categoryId, categoryObj}) => {
	const query = { user: userId, _id: categoryId }
	categoryObj.user = userId;
	await CategoryDataAccess.updateCategory({ query, categoryObj });
	return { success: 'success'}
}

module.exports.getCategory = async({ userId, categoryId }) => {
	const query = { user: userId, _id: categoryId}
	const category = await CategoryDataAccess.getCategory(query);
	return { success: category }
}

module.exports.deleteCategory = async ({ userId, categoryId }) => {
	const query = { user: userId, _id: categoryId };
	await RecordController.deleteRecordsWithCategoryId({ userId, categoryId });
	await CategoryDataAccess.deleteCategory(query);
	return { success: 'success'}
}
