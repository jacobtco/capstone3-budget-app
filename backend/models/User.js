const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, 'First Name is required']
	},
	lastName: {
		type: String,
		required: [true, 'Last Name is required']
	},
	email: {
		type: String,
		required: [true, 'Email is required'],
		unique: true
	},
	password: String,
	loginType: {
		type: String, // email of google
		required: [true, 'Login type is required'],
	}
})

module.exports = mongoose.model('User', userSchema);