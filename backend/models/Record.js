const mongoose = require('mongoose');

const recordSchema = new mongoose.Schema({
	description: {
		type: String,
		required: [true, 'Description is required']
	},
	amount: {
		type: Number,
		required: [true, 'Amount is required']
	},
	dateCreated: {
		type: Date,
		required: [true, 'Date created is required']
	},
	user: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'User',
		required: [true, 'User is required']
	},
	category: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Category',
		required: [true, 'Category is required']
	}
});

module.exports = mongoose.model('Record', recordSchema);