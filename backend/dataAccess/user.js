const UserDB = require('../models/User');

module.exports.getUser = async (query) => {
	return await UserDB.findOne(query);
}

module.exports.createUser = async (userObj) => {
	const newUser = new UserDB(userObj);
	try {
		await newUser.save();
		return true;
	} catch(err) {
		return false;
	}
}

module.exports.updateUser = async ({ query, userObj }) => {
	try {
		await UserDB.findOneAndUpdate(query, userObj);
		return true;
	} catch(err) {
		return err;
	}
}