const router = require('express').Router();
const CategoryController = require('../controllers/category');
const auth = require('../controllers/auth');

router.post('/', auth.verify, (req, res) => {
	const categoryObj = req.body;
	const userId = auth.decode(req.headers.authorization).id;
	categoryObj.user = userId;
	CategoryController.createCategory(categoryObj).then(result => res.send(result));
});

router.get('/all', auth.verify, (req, res) => {
	const userId = auth.decode(req.headers.authorization).id;
	CategoryController.getAllCategories(userId).then(result => res.send(result));
})

router.get('/all/expense', auth.verify, (req, res) => {
	const userId = auth.decode(req.headers.authorization).id;
	CategoryController.getAllExpenseCategories(userId).then(result => res.send(result));
})

router.get('/all/income', auth.verify, (req, res) => {
	const userId = auth.decode(req.headers.authorization).id;
	CategoryController.getAllIncomeCategories(userId).then(result => res.send(result));
})

router.get('/:id', auth.verify, (req, res) => {
	const userId = auth.decode(req.headers.authorization).id;
	const categoryId = req.params.id;
	CategoryController.getCategory({ userId, categoryId }).then(result => res.send(result));
})

router.put('/:id', auth.verify, (req, res) => {
	const userId = auth.decode(req.headers.authorization).id;
	const categoryId = req.params.id;
	const categoryObj = req.body;
	CategoryController.updateCategory({ userId, categoryId, categoryObj })
		.then(result => res.send(result));
})

router.delete('/:id', auth.verify, (req, res) => {
	const userId = auth.decode(req.headers.authorization).id;
	const categoryId = req.params.id;
	CategoryController.deleteCategory({ userId, categoryId })
		.then(result => res.send(result));
})

module.exports = router;