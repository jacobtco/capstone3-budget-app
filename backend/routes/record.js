const router = require('express').Router();
const RecordController = require('../controllers/record');
const auth = require('../controllers/auth');

router.get('/all/:startDateControl/:endDateControl', auth.verify, (req, res) => {
	const startDateControl = req.params.startDateControl;
	const endDateControl = req.params.endDateControl;
	const userId = auth.decode(req.headers.authorization).id;
	RecordController.getRecordsByDate({ user: userId, startDateControl, endDateControl })
		.then(result => res.send(result));
});

router.get('/all', auth.verify, (req, res) => {
	const userId = auth.decode(req.headers.authorization).id;
	RecordController.getAllRecords(userId).then(result => res.send(result));
})

router.post('/', auth.verify, (req, res) => {
	const recordObj = req.body;
	recordObj.user = auth.decode(req.headers.authorization).id;
	RecordController.createRecord(recordObj).then(result => res.send(result));
})

router.get('/total-running-balance', auth.verify, (req, res) => {
	const userId = auth.decode(req.headers.authorization).id;
	RecordController.getTotalRunningBalance(userId).then(result => res.send(result));
})

router.get('/:id', auth.verify, (req, res) => {
	const userId = auth.decode(req.headers.authorization).id;
	const recordId = req.params.id;
	RecordController.getRecord({ userId, recordId})
		.then(result => res.send(result));
})

router.put('/:id', auth.verify, (req, res) => {
	const userId = auth.decode(req.headers.authorization).id;
	const recordId = req.params.id;
	const recordObj = req.body;
	RecordController.updateRecord({ userId, recordId, recordObj })
		.then(result => res.send(result));
})

router.delete('/:id', auth.verify, (req, res) => {
	const userId = auth.decode(req.headers.authorization).id;
	const recordId = req.params.id;
	RecordController.deleteRecord({ userId, recordId })
		.then(result => res.send(result))
})

module.exports = router;
