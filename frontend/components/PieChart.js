import { Pie } from 'react-chartjs-2';

export default function PieChart({ labels, data, numberOfColors }) {
	const backgroundColor = fillBackgroundColor(numberOfColors)

	return (
		<Pie 
			data={{
				labels,
				datasets: [{
					data,
					backgroundColor
				}]
			}}
			redraw={false}
		/>
	)
}

function fillBackgroundColor(num) {
	const backgroundColor = []
	for (let i = 0; i < num; i++) {
		backgroundColor.push(colorRandomizer())
	}
	return backgroundColor;
}

function colorRandomizer() {
	return '#' + Math.floor(Math.random() * 16777215).toString(16);
}

