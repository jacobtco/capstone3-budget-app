import { Bar } from 'react-chartjs-2';
import { Spinner } from'react-bootstrap';

export default function DoubleBarChart({ 
	labels, data1, dataName1, data2, dataName2, isLoading
}) {
	if (isLoading) {
		return (
			<div className="text-center mt-4">
				<Spinner animation="border" variant="primary"/>
			</div> 
		)
	}

	if (data1.length <= 0 && data2.length <= 0) {
		return <div className='text-center mt-5'>Empty</div>
	}

	return (
		<Bar
			data={{
				labels,
				datasets:[
				{
					data: data1,
					label: dataName1,
					backgroundColor: '#dc3444'
				},
				{
					data: data2,
					label: dataName2,
					backgroundColor: '#2ca444'
				}
				]
			}}
			redraw={false}
		/>
	)
}