import { Line } from 'react-chartjs-2';
import { Spinner } from 'react-bootstrap';

export default function LineChart({ labels, data, isLoading }) {
	if (isLoading) {
		return (
			<div className="text-center mt-4">
				<Spinner animation="border" variant="primary"/>
			</div> 
		)
	}

	if (data.length <= 0 && labels.length <= 0) {
		return <div className='text-center mt-5'>Empty</div>
	}

	return (
		<Line 
			data={{
					labels,
					datasets: [{
						data,
						fill: true,
      					backgroundColor: 'rgb(255, 99, 132)',
      					borderColor: 'rgba(255, 99, 132, 0.2)',
					}]
			}}
			redraw={false}
			options={{
				plugins: {
					legend: {
						display: false
					}
				}
			}}
		/>
	)
};