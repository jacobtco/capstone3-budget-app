import Head from 'next/head';
import { useState, useContext, Fragment, useEffect } from 'react';
import { Card, Form, Button, InputGroup } from 'react-bootstrap';
import Link from 'next/link';
import UserContext from '../../contexts/UserContext';
import dateHelper from '../../helpers/dateHelpers';

export default function index() {
	const { user } = useContext(UserContext);
	const [expenseCategories, setExpenseCategories] = useState([]);
	const [incomeCategories, setIncomeCategories] = useState([]);

	const [categoryType, setCategoryType] = useState('');
	const [chosenCategory, setChosenCategory] = useState('');
	const [description, setDescription] = useState('');
	const [amount, setAmount] = useState('');
	const [dateCreated, setDateCreated] =
		useState(dateHelper.dateObjToDateControl(new Date()));

	const [isDisabled, setIsDisabled] = useState(true);
	const [isLoading, setIsLoading] = useState(false);
	const [reset, setReset] = useState(false);

	const backendAPI = process.env.NEXT_PUBLIC_BACKEND_API || '';

	useEffect(() => {
		async function getCategories() {
			const token = user.token;
			if (token === null) return;

			const expenseRoute = `${backendAPI}/api/categories/all/expense`;
			const incomeRoute = `${backendAPI}/api/categories/all/income`;
			const options = { headers: {
				'Authorization': `Bearer ${token}`
			}};
			
			const expenseCategoriesRes = await fetch(expenseRoute, options);
			const expenseCategoriesData = await expenseCategoriesRes.json();
			setExpenseCategories(expenseCategoriesData.success);

			const incomeCategoriesRes = await fetch(incomeRoute, options);
			const incomeCategoriesData = await incomeCategoriesRes.json();
			setIncomeCategories(incomeCategoriesData.success);
		}
		
		getCategories();
	}, [user]);

	useEffect(() => {
		if (description !== '' && amount !== '' && chosenCategory !== '') {
			setIsDisabled(false);
		} else {
			setIsDisabled(true);
		}
	}, [description, amount, chosenCategory])

	async function createRecord(e) {
		e.preventDefault();
		setIsLoading(true);
		const recordBody = {
			description,
			amount,
			dateCreated,
			category: chosenCategory
		}
		const token = user.token;
		const recordRes = await fetch(`${backendAPI}/api/records`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify(recordBody)
		});
		const categoryData = await recordRes.json();

		if (categoryData.success) {
			alert(categoryData.success);
			setCategoryType('');
			setChosenCategory('');
			setDescription('');
			setAmount('');
			setReset(true);
			setReset(false);
			setDateCreated(dateHelper.dateObjToDateControl(new Date()));
		} else if (categoryData.fail) {
			alert(categoryData.fail);
		}
		setIsLoading(false);
	}

	return (
		<Fragment>
			<Head>
				<title>New Record | Power Budget Tracker</title>
			</Head>
			<h4>New Record</h4>
			<Card>
				<Card.Header>Record Information</Card.Header>
				<Card.Body>
					<Form onSubmit={createRecord}>
					{ reset
						? ''
						: <Fragment>
								<Form.Group>
									<Form.Label>Category Type</Form.Label>
									<Form.Control
										as="select"
										defaultValue="Select Category"
										onChange={(e) => setCategoryType(e.target.value)}
										>
										<option disabled>Select Category</option>
										<option value="expense">expense</option>
										<option value="income">income</option>
									</Form.Control>
								</Form.Group>

								<Form.Group>
									<Form.Label>Category Name:</Form.Label>
									<Form.Control 
										as="select"
										defaultValue="Select Category Type First"
										onChange={(e) => setChosenCategory(e.target.value)}
										disabled={categoryType === ''}
										>
										<option disabled>Select Category Type First</option>
										{ categoryType === 'income'
											? <Fragment>
													{incomeCategories.map(cat => {
														return (
															<option
																key={cat._id}
																value={cat._id}
															>
																{cat.name}
															</option>
														)
													})}
												</Fragment>
											: <Fragment>
													{expenseCategories.map(cat => {
														return (
															<option
																key={cat._id}
																value={cat._id}
															>
																{cat.name}
															</option>
														)
													})}
												</Fragment>
										}
									</Form.Control>
								</Form.Group>
							</Fragment>
						}

						<Form.Group>
							<Form.Label>Record Description:</Form.Label>
							<Form.Control 
							type="input"
							placeholder="Enter Category Name"
							value={description}
							onChange={(e) => setDescription(e.target.value)}
							/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Amount:</Form.Label>
							<InputGroup>
								<InputGroup.Prepend>
							    	<InputGroup.Text id="basic-addon1">php</InputGroup.Text>
							    </InputGroup.Prepend>
							    <Form.Control 
								type="number"
								placeholder="0"
								value={amount}
								onChange={(e) => setAmount(e.target.value)}
								/>
							</InputGroup>
						</Form.Group>

						<Form.Group>
							<Form.Label>Date:</Form.Label>
							<InputGroup>
							    <Form.Control 
								type="date"
								value={dateCreated}
								onChange={(e) => setDateCreated(e.target.value)}
								/>
							</InputGroup>
						</Form.Group>
						
						<Button type="submit" disabled={isDisabled}>
							{ isLoading
								? <span>
										<span 
											className="spinner-border spinner-border-sm"
											role="status"
											aria-hidden="true">
										</span>
										<span> Sending...</span>
									</span>
								: 'Submit'
							}
						</Button>
					</Form>
				</Card.Body>
			</Card>
			<Link href="/records">
				<Button className="btn-info d-block w-40 mt-2" disabled={isLoading}>Go Back</Button>
			</Link>
		</Fragment>
	)
}
