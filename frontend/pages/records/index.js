import Head from 'next/head';
import { Fragment, useEffect, useState, useContext } from 'react';
import { InputGroup, Button, FormControl, Spinner } from 'react-bootstrap';
import Link from 'next/link';

import dateHelper from '../../helpers/dateHelpers';
import alertHelper from '../../helpers/alertHelpers';
import UserContext from '../../contexts/UserContext';
import RecordCard from '../../components/RecordCard';
import FromToDateControl from '../../components/FromToDateControl';

export default function index() {
	const [filter, setFilter] = useState('all');
	const [searchWords, setSearchWords] = useState('');

	const [records, setRecords] = useState([]);
	const [hashCardRecords, setHashCardRecords] = useState([]);
	const [displayRecords, setDisplayRecords] = useState([]);

	const [runningBalance, setRunningBalance] = useState(0);

	const [startDate, setStartDate] = useState('');
	const [endDate, setEndDate] = useState('');

	const [isLoading, setIsLoading] = useState(true);

	const [historicalBalance, setHistoricalBalance] = useState(
			<Spinner animation="border" variant="primary" size="sm"/>
	)

	const { user } = useContext(UserContext);

	const backendAPI = process.env.NEXT_PUBLIC_BACKEND_API || '';

	function recordsToCards(records) {
		// go in reverse order to see the latest
		const recordCards = [];
		records.forEach(record => {
			recordCards.unshift(
				<RecordCard 
					record={record} 
					key={record._id} 
					deleteRecord={(recordId) => {
						alertHelper.confirm(() => deleteRecord(recordId));
					}}
				/>
			)}
		);
		return recordCards
	}

	useEffect(() => {
		const generatedControlDate = dateHelper.generateStartEndControlDate();
		setStartDate(generatedControlDate.startDate);
		setEndDate(generatedControlDate.endDate);
	}, []);

	useEffect(() => {
		getTotalRunningBalance()
	}, [user]);

	useEffect(() => {
		getRecords();
	}, [user, startDate, endDate]);

	useEffect(() => {
		const hash = {}
			// all
			hash.all = recordsToCards(records);
			// expense and income
			hash.expense = [];
			hash.income = [];
			records.forEach(record => {
				if (record.category.budgetType === 'expense') {
					hash.expense.push(record);
				} else if (record.category.budgetType === 'income') {
					hash.income.push(record);
				}
			});

			hash.expense = recordsToCards(hash.expense);
			hash.income = recordsToCards(hash.income);

			// check if empty
			Object.keys(hash).forEach(key => {
				if (hash[key].length <= 0) {
					hash[key] = <div className='text-center mt-5'>Empty</div>
				}
			});
		setHashCardRecords(hash);
		calculateRunningBalance(records, setRunningBalance);
	}, [records]);

	useEffect(() => {
		if (searchWords === '') {
			setDisplayRecords(hashCardRecords[filter])
		} else {
			const filteredCards = hashCardRecords[filter].filter(card => {
				const description = card.props.record.description.toLowerCase();
				return description.includes(searchWords.toLowerCase());
			});
			setDisplayRecords(filteredCards);
		}
	}, [searchWords, hashCardRecords, filter])

	function calculateRunningBalance(records, setRunningBalance) {
		let total = 0;
		records.forEach(record => {
			if (record.category.budgetType === 'expense') {
				total -= record.amount;
			} else if (record.category.budgetType === 'income') {
				total += record.amount;
			}
		});
		setRunningBalance(total);
	}

	async function getTotalRunningBalance() {
		const token = user.token;
		if (token === null) return;

		const route = `${backendAPI}/api/records/total-running-balance`;
		console.log(route);

		const balanceRes = await fetch(route, {
			headers: {
			'Authorization': `Bearer ${token}`
			}
		});
	
		const balanceData = await balanceRes.json();
		console.log(balanceData)
		setHistoricalBalance(balanceData.success);
	}

	async function deleteRecord(recordId) {
		const token = user.token;
		const route = `${backendAPI}/api/records/${recordId}`;
		const recordRes = await fetch(route, {
			method: 'DELETE',
			headers: {
				'Authorization': `Bearer ${token}`
			}
		});
		const recordData = await recordRes.json();
		if (recordData.success) {
			await getRecords();
			alert(recordData.success)
		} else {
			alert(recordData.fail);
		}
	}

	async function getRecords() {
		const token = user.token;
		if (token === null) return;	
		if (startDate === '' || endDate === '') return;
		const route = `${backendAPI}/api/records/all/${startDate}/${endDate}`;
		const recordsRes = await fetch(route, {
			headers: {
				'Authorization': `Bearer ${token}`
			}
		});

		const recordsData = await recordsRes.json();
		setRecords(recordsData.success);
		setIsLoading(false);
	}
		

	return (
		<Fragment>
			<Head>
				<title>Records | Power Budget Tracker</title>
			</Head>
			<h4>Records</h4>
			<InputGroup className="mb-2">
				<InputGroup.Prepend>
					<Link href="/records/new">
						<Button variant="success">Add</Button>
					</Link>
				</InputGroup.Prepend>
				<FormControl
					type="input"
					placeholder="Search Records"
					value={searchWords}
					onChange={(e) => setSearchWords(e.target.value)}
				/>
				<InputGroup.Append>
					<FormControl 
						as="select"
						onChange={(e) => setFilter(e.target.value)}
					>
						<option value="all">all</option>
						<option value="expense">expense</option>
						<option value="income">income</option>
					</FormControl>
				</InputGroup.Append>
			</InputGroup>

			<FromToDateControl 
				startDate={startDate}
				endDate={endDate}
				setStartDate={setStartDate}
				setEndDate={setEndDate}
			/>

			{ isLoading
				? <div className="text-center mt-4">
						<Spinner animation="border" variant="primary"/>
					</div> 
				: <Fragment>
						<h5 className="text-center">
							{'Total Running Balance: '}
							<span 
								className={runningBalance > 0 ? 'text-success' : 'text-danger'}
							>
								{historicalBalance}
							</span>
						</h5>
						<h5 className="text-center">
								Date Related Running Balance:
							<span 
								className={runningBalance > 0 ? 'text-success' : 'text-danger'}
							>
								{` ${runningBalance}`}
							</span>
						</h5>
						 {displayRecords}
					</Fragment>
			}
			
		</Fragment>
	)
}