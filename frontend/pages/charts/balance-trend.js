// get recrods given date
// labels will be based on days, x-axis
// add budget on each day, y-axis
// pass into lineChart
// create loading screen
import Head from 'next/head';
import { Fragment, useState, useEffect, useContext } from 'react';
import { Form, Row, Col } from 'react-bootstrap';
import dateHelper from '../../helpers/dateHelpers';
import UserContext from '../../contexts/UserContext';
import LineChart from '../../components/LineChart';
import FromToDateControl from '../../components/FromToDateControl'

export default function balanceTrend() {
	const [startDate, setStartDate] = useState('');
	const [endDate, setEndDate] = useState('');

	const [dates, setDates] = useState([]);
	const [trends, setTrends] = useState([]);

	const [isLoading, setIsLoading] = useState(true);

	const { user } = useContext(UserContext);
	const backendAPI = process.env.NEXT_PUBLIC_BACKEND_API || '';

	useEffect(() => {
		const generatedControlDate = dateHelper.generateStartEndControlDate();
		setStartDate(generatedControlDate.startDate);
		setEndDate(generatedControlDate.endDate);
	}, []);

	useEffect(() => {
		if (startDate !== '', endDate !== '') {
			generateBudgetTrend();
		}
	}, [startDate, endDate, user.token])

	function sortRecordsByDate(record) {
		record.sort((r1, r2) => {
			return new Date(r1.dateCreated) - new Date(r2.dateCreated)
		});
	}

	async function generateBudgetTrend() {
		const token = user.token;
		if (token === null) return;

		const route = `${backendAPI}/api/records/all/${startDate}/${endDate}`;
		const recordsRes = await fetch(route, {
			headers: {
				'Authorization': `Bearer ${token}`
			}
		});
		const recordsData = await recordsRes.json();
		const records = recordsData.success;

		sortRecordsByDate(records);

		const dates = [];
		const trends = [];
		let counter = 0;

		records.forEach(record => {
			const date = dateHelper
				.dateObjToDateControl(new Date(record.dateCreated));

			if (dates[counter] === undefined) {
				dates[counter] = date;
				trends[counter] = 0;
			}
			if (dates[counter] !== date) {
				counter += 1;
				dates[counter] = date;
				trends[counter] = trends[counter - 1];
			}

			if (record.category.budgetType === 'income') {
				trends[counter] += record.amount;
			} else if (record.category.budgetType === 'expense') {
				trends[counter] -= record.amount;
			}
		});

		setTrends(trends);
		setDates(dates);
		setIsLoading(false);
	}

	return (
		<Fragment>
			<Head>
				<title>Balance Trend | Power Budget Tracker</title>
			</Head>
			<h4>Balance Trend</h4>
			<FromToDateControl
				startDate={startDate}
				setStartDate={setStartDate}
				endDate={endDate}
				setEndDate={setEndDate}
			/>
			<LineChart labels={dates} data={trends} isLoading={isLoading}/>
		</Fragment>
	)
}