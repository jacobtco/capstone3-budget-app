import Head from 'next/head'
import { Fragment, useEffect, useContext, useState } from 'react';
import UserContext from '../../contexts/UserContext';
import DoubleBarChart from '../../components/DoubleBarChart';
import FromToDateControl from '../../components/FromToDateControl';
import dateHelper from '../../helpers/dateHelpers';

export default function monthlyReport() {
	const { user } = useContext(UserContext);
	const backendAPI = process.env.NEXT_PUBLIC_BACKEND_API || ''

	const [records, setRecords] = useState([]);
	const [startDate, setStartDate] = useState('');
	const [endDate, setEndDate] = useState('');
	const [months, setMonths] = useState([]);
	const [monthlyIncome, setMonthlyIncome] = useState([]);
	const [monthlyExpense, setMontlyExpense] = useState([]);

	const [isLoading, setIsLoading] = useState(true);

	function setDateAndYearOnly(dateControl) {
		const [year, month, date] = dateControl.split('-');
		return `${year}-${month}-01`;
	}

	function modSetStartDate(dateControl) {
		setStartDate(setDateAndYearOnly(dateControl));
	}

	function modSetEndDate(dateControl) {
		setEndDate(setDateAndYearOnly(dateControl));
	}

	useEffect(() => {
		const generatedDates = dateHelper.generateStartEndControlDate(5);
		modSetStartDate(generatedDates.startDate);
		modSetEndDate(generatedDates.endDate);
	}, [])

	useEffect(() =>{
		getRecords();
	}, [user.token, startDate, endDate])

	useEffect(() => {
		setMonthlyDate();
	}, [records])

	function setMonthlyDate() {
		// sort ascending data
		// counter
		// if undefined place month
		// if month doesn't match, add counter one and place month
		// check if income or expnse then place accordingly
		const months = [];
		const monthlyIncome = [];
		const monthlyExpense = [];

		records.sort((r1, r2) => new Date(r1.dateCreated) - new Date(r2.dateCreated));
		let counter = 0;
		records.forEach(record => {
			const mAndY = dateHelper.getMonthAndYearControl(new Date(record.dateCreated));
			if (months[counter] === undefined) {
				months[counter] = mAndY;
				monthlyIncome[counter] = 0;
				monthlyExpense[counter] = 0;
			}
			if (months[counter] !== mAndY) {
				counter += 1
				months[counter] = mAndY;
				monthlyIncome[counter] = 0;
				monthlyExpense[counter] = 0;
			}
			const budgetType = record.category.budgetType;
			if (budgetType === 'income') {
				monthlyIncome[counter] += record.amount;
			} else if (budgetType === 'expense') {
				monthlyExpense[counter] += record.amount;
			}
				
		});
		setMonths(months);
		setMonthlyIncome(monthlyIncome);
		setMontlyExpense(monthlyExpense);
	}

	async function getRecords() {
		const token = user.token;
		if (token === null) return;	
		if (startDate === '' || endDate === '') return;
		const newEndDate = dateHelper.dateControlLastDay(endDate);
		const route = `${backendAPI}/api/records/all/${startDate}/${newEndDate}`;
		const recordsRes = await fetch(route, {
			headers: {
				'Authorization': `Bearer ${token}`
			}
		});

		const recordsData = await recordsRes.json();
		setRecords(recordsData.success);
		setIsLoading(false);
	}

	return (
		<Fragment>
			<Head>
				<title>Monthly Report | Power Budget Tracker</title>
			</Head>
			<h4>Monthly Report</h4>
			<FromToDateControl 
				startDate={startDate}
				endDate={endDate}
				setStartDate={modSetStartDate}
				setEndDate={modSetEndDate}
			/>

			<DoubleBarChart
				labels={months}
				data1={monthlyExpense}
				dataName1={'Expenses'}
				data2={monthlyIncome}
				dataName2={'Income'}
				isLoading={isLoading}
			/>
		</Fragment>
	)
}