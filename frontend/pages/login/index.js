import Head from 'next/head';
import { useState, useEffect, useContext, Fragment } from 'react';
import { useRouter } from 'next/router';
import { Form, Button } from 'react-bootstrap';
import Link from 'next/link';
import { GoogleLogin } from 'react-google-login';
import UserContext from '../../contexts/UserContext';

export default function index() {
	const router = useRouter();
	const { user, setUser } = useContext(UserContext);
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isDisabled, setIsDisabled] = useState(true);
	const [isLoading, setIsLoading] = useState(false);

	useEffect(() => {
		if (user.token !== null) {
			router.push('/');
		}
	}, [user]);

	useEffect(() => {
		const isValidEmail = !!(email.match(/.+@.+/));
		if (isValidEmail && password !== '') {
			setIsDisabled(false);
		} else {
			setIsDisabled(true);
		}
	}, [email, password])

	const backendAPI = process.env.NEXT_PUBLIC_BACKEND_API || '';

	async function onLogin(e) {
		e.preventDefault();
		setIsLoading(true);
		const loginRes = await fetch(`${backendAPI}/api/users/login`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({ email, password })
		});
		const loginData = await loginRes.json();
		if (loginData.success) {
			const token = loginData.success;
			alert('success');
			setUser({ token });
			localStorage.setItem('token', token);
			router.push('/');
		} else if (loginData.fail) {
			alert(loginData.fail);
			setIsLoading(false);
		}
	}

	async function googleLogin(res) {
		setIsLoading(true);
		const loginRes = await fetch(`${backendAPI}/api/users/google-login`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				googleToken: res.tokenId
			})
		});
		const loginData = await loginRes.json();
		if (loginData.success) {
			const token = loginData.success;
			alert('success');
			setUser({ token });
			localStorage.setItem('token', token);
			router.push('/');
		} else if (loginData.fail) {
			alert(loginData.fail);
			setIsLoading(false);
		}
	}

	return (
		<Fragment>
			<Head>
				<title>Login | Power Budget Tracker</title>
			</Head>
			<Form onSubmit={onLogin}>
				<h4>Power Budget Tracking</h4>
				<p>Login by using your Google Account or your Registered Email.</p>
				<Form.Group>
					<Form.Label>Email:</Form.Label>
					<Form.Control 
						type="email"
						placeholder="email@mail.com"
						value={email}
						onChange={(e) => setEmail(e.target.value)}
					/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Password:</Form.Label>
					<Form.Control 
						type="password"
						placeholder="******"
						value={password}
						onChange={(e) => setPassword(e.target.value)}
					/>
				</Form.Group>
				{isLoading
					? <Button className="w-100" disabled>
							<span 
						 		className="spinner-border spinner-border-sm"
						 		role="status"
						 		aria-hidden="true">		
						 	</span>
	  						<span className="visually-hidden"> Logging In...</span>
						</Button>
					: <Button className="w-100" type="submit" disabled={isDisabled}>Submit</Button>
				}
				<GoogleLogin
					clientId={process.env.NEXT_PUBLIC_GOOGLE_CLIENT_ID}
					buttonText="Login Using Google"
					onSuccess={googleLogin}
					onFailure={googleLogin}
					cookiePolicy={'single_host_origin'}
					className="w-100 d-flex justify-content-center"
				/>
				<div className="text-center mt-2">
					<p>If you haven't registered yet, 
						<Link href="/register"><a> Sign Up Here!</a></Link>
					</p>
				</div>
			</Form>
		</Fragment>
	)
}
