import { Fragment } from 'react';
import NavBar from '../components/NavBar';
import { Container } from 'react-bootstrap';
import { UserProviderWrapper } from '../contexts/UserContext';
import '../styles/globals.css'
import 'bootstrap/dist/css/bootstrap.min.css';

function MyApp({ Component, pageProps }) {
  return (
  	<Fragment>
      <UserProviderWrapper>
        <NavBar />
          <Container>
              <Component {...pageProps} />
          </Container>
      </UserProviderWrapper>
  	</Fragment>
  )
}

export default MyApp
