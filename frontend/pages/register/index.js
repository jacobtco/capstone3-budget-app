import Head from 'next/head';
import { useState, useEffect, useContext, Fragment } from 'react';
import { useRouter } from 'next/router';
import { Form, Button } from 'react-bootstrap';
import UserContext from '../../contexts/UserContext';

export default function index() {
	const router = useRouter();
	const { user } = useContext(UserContext);
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [confirmatoryPassword, setConfirmatoryPassword] = useState('');
	const [isDisabled, setIsDisabled] = useState(true);
	const [isLoading, setIsLoading] = useState(false);

	const backendAPI = process.env.NEXT_PUBLIC_BACKEND_API || '';

	useEffect(() => {
		console.log(user);
		if (user.token !== null) {
			router.push('/');
		}
	}, [user]);

	useEffect(() => {
		const isFilledIn = firstName !== '' && lastName !== '' &&
			email !== '' && password !== '' && confirmatoryPassword !== ''
		const isMatchingPasswords = password === confirmatoryPassword;
		const isValidEmail = !!(email.match(/.+@.+/));
		const isSixCharactersLong = password.length >= 6;

		if (isFilledIn && isMatchingPasswords && isValidEmail 
			&& isSixCharactersLong) {
			setIsDisabled(false);
		} else {
			setIsDisabled(true);
		}
	}, [firstName, lastName, email, password, confirmatoryPassword])

	async function onRegister(e) {
		e.preventDefault();
		setIsLoading(true);
		const registerRes = await fetch(`${backendAPI}/api/users/register`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({ firstName, lastName, email, password })
		});

		const registerData = await registerRes.json();

		if (registerData.success) {
			alert(registerData.success);
			router.push('/login');
		} else if (registerData.fail) {
			alert(registerData.fail);
			setIsLoading(false);
		}
	}

	return (
		<Fragment>
			<Head>
				<title>Register | Power Budget Tracker</title>
			</Head>
			<Form onSubmit={onRegister}>
				<h4>Register</h4>

				<Form.Group>
					<Form.Label>First Name</Form.Label>
					<Form.Control 
						type="input"
						placeholder="First Name"
						value={firstName}
						onChange={(e) => setFirstName(e.target.value)}
					/>
				</Form.Group>

				<Form.Group>
					<Form.Label>Last Name</Form.Label>
					<Form.Control
						type="input"
						placeholder="Last Name"
						value={lastName}
						onChange={(e) => setLastName(e.target.value)}
					/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Email</Form.Label>
					<Form.Control 
						type="email"
						placeholder="example@mail.com"
						value={email}
						onChange={(e) => setEmail(e.target.value)}
					/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Password</Form.Label>
					<Form.Control
						type="password"
						placeholder="Set Password"
						value={password}
						onChange={(e) => setPassword(e.target.value)}
					/>
					<Form.Text className="text-muted">
						Must be 6 characters long.
					</Form.Text>
				</Form.Group>
				<Form.Group>
					<Form.Label>Confirm Password</Form.Label>
					<Form.Control
						type="password"
						placeholder="Confirm Password"
						value={confirmatoryPassword}
						onChange={(e) => setConfirmatoryPassword(e.target.value)}
					/>
				</Form.Group>
				{ isLoading
					? <Button disabled>
						 	<span 
						 		className="spinner-border spinner-border-sm"
						 		role="status"
						 		aria-hidden="true">		
						 	</span>
	  						<span className="visually-hidden"> Registering...</span>
						</Button>
					: <Button type="submit" disabled={isDisabled}>Register</Button>
				}
			</Form>
		</Fragment>
	)
}
