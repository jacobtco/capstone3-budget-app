import Head from 'next/head';
import { useState, useContext, Fragment, useEffect } from 'react';
import { Card, Form, Button } from 'react-bootstrap';
import Link from 'next/link';
import UserContext from '../../contexts/UserContext';

export default function index() {
	const { user } = useContext(UserContext);
	const [name, setName] = useState('');
	const [budgetType, setBudgetType] = useState('');
	const [isDisabled, setIsDisabled] = useState(true);
	const [isLoading, setIsLoading] = useState(false);

	const backendAPI = process.env.NEXT_PUBLIC_BACKEND_API || '';

	useEffect(() => {
		if (name !== '' && budgetType !== '') {
			setIsDisabled(false);
		} else {
			setIsDisabled(true);
		}
	}, [name, budgetType])

	async function createCategory(e) {
		e.preventDefault();
		setIsLoading(true);
		const token = user.token;
		const categoryRes = await fetch(`${backendAPI}/api/categories`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({ name, budgetType })
		});
		const categoryData = await categoryRes.json();

		if (categoryData.success) {
			alert(categoryData.success);
		} else if (categoryData.fail) {
			alert(categoryData.fail);
		}
		setIsLoading(false);
		setName('');
	}

	return (
		<Fragment>
			<Head>
				<title>New Category | Power Budget Tracker</title>
			</Head>
			<h4>New Category</h4>
			<Card>
				<Card.Header>Category Information</Card.Header>
				<Card.Body>
						<Form onSubmit={createCategory}>
							<Form.Group>
								<Form.Label>Category Name:</Form.Label>
								<Form.Control 
									type="input"
									placeholder="Enter Category Name"
									value={name}
									onChange={(e) => setName(e.target.value)}
								/>
							</Form.Group>
							<Form.Group>
								<Form.Label>Category Type:</Form.Label>
								<Form.Check 
									type="radio"
									label="Income"
									name="categoryType"
									id="income"
									onChange={(e) => setBudgetType(e.target.id)}
									size="lg"
								/>
								<Form.Check
									type="radio"
									label="Expense"
									name="categoryType"
									id="expense"
									onChange={(e) => setBudgetType(e.target.id)}
								/>
							</Form.Group>
							<Button type="submit" disabled={isDisabled}>
								{ isLoading
									? <span>
											<span 
												className="spinner-border spinner-border-sm"
												role="status"
												aria-hidden="true">
											</span>
											<span> Sending...</span>
										</span>
									: 'Submit'
								}
							</Button>
						</Form>
				</Card.Body>
			</Card>
			<Link href="/categories">
				<Button className="btn-info d-block w-40 mt-2" disabled={isLoading}>Go Back</Button>
			</Link>
		</Fragment>
	)
}
