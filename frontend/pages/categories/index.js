import Head from 'next/head';
import { Fragment, useState, useEffect, useContext } from 'react';
import { Table, Row, Col, Button, InputGroup, FormControl, Spinner } from 'react-bootstrap';
import Link from 'next/link';
import UserContext from '../../contexts/UserContext';

export default function index() {
	const [categories, setCategories] = useState([]);
	const [displayCategories, setDisplayCategories] = useState([]);
	const [tableBody, setTableBody] = useState([]);
	const [filteredCategories, setFilteredCategories] = useState({
		all: [],
		income: [],
		expense: []
	})

	const [searchWords, setSearchWords] = useState('');
	const [filter, setFilter] = useState('all');
	const [isLoading, setIsLoading] = useState(true);

	const { user } = useContext(UserContext);

	const backendAPI = process.env.NEXT_PUBLIC_BACKEND_API || '';

	useEffect(() => {
		getCategories(setCategories);
	}, [user]);

	useEffect(() => {
		constructFilteredCategories(categories, setFilteredCategories);
	}, [categories]);

	useEffect(() => {
		if (searchWords === '') {
			setDisplayCategories(filteredCategories[filter])
		} else {
			setDisplayCategories(filteredCategories[filter].filter(category => {
				const categoryName = category.name.toLowerCase();
				const searchWordsLowerCase = searchWords.toLowerCase();
				return categoryName.includes(searchWordsLowerCase);
			}));
		}
	}, [searchWords, filter, filteredCategories])

	useEffect(() => {
		tablerizeCategories(displayCategories, setCategories, setTableBody, deleteCategory)
	}, [displayCategories])

	function constructFilteredCategories(categories, setFilteredCategories) {
		const hashContainer = {
			all: categories,
			income: [],
			expense: []
		}

		categories.forEach(category => {
			hashContainer[category.budgetType].push(category)
		});

		setFilteredCategories(hashContainer);
	}

	function sortCategories(categories) {
		categories.sort((c1, c2) => {
			const c1Name = c1.name.toLowerCase();
			const c2Name = c2.name.toLowerCase();
			if (c1Name < c2Name) return -1;
			if (c1Name > c2Name) return 1;
			return 0; 
		})
	}

	async function deleteCategory(categoryId, setCategories) {
		const confirmation = 
			confirm('This will also delete associated records, proceed?');
		if (confirmation === false) return;

		const route = `${backendAPI}/api/categories/${categoryId}`;
		const token = user.token;

		const deleteCatRes = await fetch(route, {
			method: 'DELETE',
			headers: {
				'Authorization': `Bearer ${token}`
			}
		});
		const deleteCatData = await deleteCatRes.json();

		if (deleteCatData.success) {
			await getCategories(setCategories);
			alert('Category Successfully Deleted');
		}
	}

	function tablerizeCategories(categories, setCategories, setTableBody) {
		setTableBody(categories.map(category => {
			const textColor = category.budgetType === 'income'
				? 'text-success'
				: 'text-danger'

			return (
				<tr key={category._id}>
					<td className="align-middle">{category.name}</td>
					<td className={`align-middle ${textColor}`}>
						{category.budgetType[0].toUpperCase() + category.budgetType.slice(1)}
					</td>
					<td>
						<Row>
							<Link href={`/categories/edit/${category._id}`}>
								<Button variant="warning" className="w-100">Edit</Button>
							</Link>
						</Row>
						<Row className="mt-1">
							<Button 
								variant="danger"
								className="w-100"
								onClick={() => deleteCategory(category._id, setCategories)}
							>
								Delete
							</Button>
						</Row>
					</td>
				</tr>
			)
		}));
	}

	async function getCategories(setCategories) {
		const token = user.token;
		if (token === null) return;
		const categoriesRes = await fetch(`${backendAPI}/api/categories/all`, {
			headers: {
				'Authorization': `Bearer ${token}`
			}
		});
		const categoriesData = await categoriesRes.json();
		const categories = categoriesData.success;
		sortCategories(categories)
		setCategories(categories);
		setIsLoading(false);
	}

	return (
		<Fragment>
			<Head>
				<title>Categories | Power Budget Tracker</title>
			</Head>
			<h4>Categories</h4>
			<InputGroup className="mb-2">
				<InputGroup.Prepend>
					<Link href="/categories/new">
						<Button>Add</Button>
					</Link>
				</InputGroup.Prepend>
				<FormControl
					type="input"
					placeholder="Search Records"
					value={searchWords}
					onChange={(e) => setSearchWords(e.target.value)}
				/>
				<InputGroup.Append>
					<FormControl 
						as="select"
						onChange={(e) => setFilter(e.target.value)}
					>
						<option value="all">all</option>
						<option value="expense">expense</option>
						<option value="income">income</option>
					</FormControl>
				</InputGroup.Append>
			</InputGroup>

			<Table striped bordered hover>
				<thead>
					<tr>
						<th>Category</th>
						<th>Type</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{ tableBody }
				</tbody>
			</Table>
				{ isLoading
						? <div className="text-center mt-4">
								<Spinner animation="border" variant="primary"/>
							</div> 
						: tableBody.length <= 0
							? <div className='text-center mt-5'>Empty</div>
							: ''
				}
		</Fragment>
	)
}