import Head from 'next/head'
import { Fragment, useEffect, useContext } from 'react';
import { useRouter } from 'next/router';
import UserContext from '../../contexts/UserContext';

export default function index() {
	const { unsetUser } = useContext(UserContext);
	const router = useRouter();

	useEffect(() => {
		unsetUser();
		router.push('/login');
	}, [])

	return (
		<Fragment>
			<Head>
				<title>Logout | Power Budget Tracker</title>
			</Head>
		</Fragment>
	)
}