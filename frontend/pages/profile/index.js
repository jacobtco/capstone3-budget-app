import Head from 'next/head';
import { useState, useContext, Fragment, useEffect } from 'react';
import { Card, Form, Button } from 'react-bootstrap';
import Link from 'next/link';
import { useRouter } from 'next/router';
import UserContext from '../../contexts/UserContext';

export default function index() {
	const router = useRouter();
	const { user } = useContext(UserContext);

	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [loginType, setLoginType] = useState('');
	const [prevLoginType, setPrevLoginType] = useState('');
	const [password, setPassword] = useState('');
	const [confirmPassword, setConfirmPassword] = useState('');

	const [isDisabled, setIsDisabled] = useState(true);
	const [isLoading, setIsLoading] = useState(false);

	const backendAPI = process.env.NEXT_PUBLIC_BACKEND_API || '';

	useEffect(() => {
		setUserDetails();
	}, [user.token])

	useEffect(() => {
		const isChangingToEmail = prevLoginType === 'google' && loginType === 'email';

		if (password === confirmPassword && 
			(password.length === 0 || password.length >= 6)) 
		{
			if (isChangingToEmail && password === '') {
				setIsDisabled(true);
			} else {
				setIsDisabled(false);
			}
		} else {
			setIsDisabled(true);
		}
	}, [password, confirmPassword, loginType])

	async function setUserDetails() {
		const token = user.token;
		if (token === null) return;
		const userRes = await fetch(`${backendAPI}/api/users`, {
			headers: {
				'Authorization': `Bearer ${token}`
			}
		});
		const userData = await userRes.json();
		const userDetails = userData.success;
		setFirstName(userDetails.firstName);
		setLastName(userDetails.lastName);
		setEmail(userDetails.email);
		setLoginType(userDetails.loginType);
		setPrevLoginType(userDetails.loginType);
	}

	async function editUser(e) {
		e.preventDefault();
		setIsLoading(true);
		const token = user.token;
		const userUpdateRes = await fetch(`${backendAPI}/api/users`, {
			method: 'PUT',
			headers: {
				'Authorization': `Bearer ${token}`,
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				firstName,
				lastName,
				email,
				loginType,
				password 
			})
		});
		const userUpdateData = await userUpdateRes.json();
		
		if (userUpdateData.success) {
			alert('success');
			await setUserDetails();
		} else {
			alert('fail');
		}
		setIsLoading(false);
	}

	return (
		<Fragment>
			<Head>
				<title>My Profile | Power Budget Tracker</title>
			</Head>
			<h4>My Profile</h4>
			<Card>
				<Card.Header>Profile Information</Card.Header>
				<Card.Body>
						<Form onSubmit={editUser}>
							<Form.Group>
								<Form.Label>First Name:</Form.Label>
								<Form.Control 
									type="input"
									placeholder="First Name"
									value={firstName}
									onChange={(e) => setFirstName(e.target.value)}
								/>
							</Form.Group>

							<Form.Group>
								<Form.Label>Last Name:</Form.Label>
								<Form.Control 
									type="input"
									placeholder="Last Name"
									value={lastName}
									onChange={(e) => setLastName(e.target.value)}
								/>
							</Form.Group>

							<Form.Group>
								<Form.Label>Email:</Form.Label>
								<Form.Control 
									type="input"
									placeholder="Email"
									value={email}
									disabled
								/>
								<Form.Text className="text-muted">
									Cannot be changed
								</Form.Text>
							</Form.Group>

							<Form.Group>
								<Form.Label>Login Type:</Form.Label>
								<Form.Check 
									type="radio"
									label="Email"
									name="loginType"
									id="email"
									checked={loginType === 'email'}
									onChange={(e) => setLoginType(e.target.id)}
								/>
								<Form.Check
									type="radio"
									label="Google"
									name="loginType"
									id="google"
									checked={loginType === 'google'}
									disabled={prevLoginType === 'email'}
									onChange={(e) => setLoginType(e.target.id)}
								/>

								<Form.Text className="text-muted">
									Can only change from Google Login to Email Login
								</Form.Text>
							</Form.Group>

							<Form.Group>
								<Form.Label>Password:</Form.Label>
								<Form.Control 
									autoComplete="off" 
									type="password"
									placeholder={
										(prevLoginType === 'google' && loginType === 'email')
											? "Required"
											: "Not required"
									}
									value={password}
									onChange={(e) => setPassword(e.target.value)}
								/>
								<Form.Text className="text-muted">
									Should be at least 6 characters long.
								</Form.Text>
							</Form.Group>

							<Form.Group>
								<Form.Label>Confirm Password:</Form.Label>
								<Form.Control 
									type="password"
									placeholder={
										(prevLoginType === 'google' && loginType === 'email')
											? "Required"
											: "Not required"
									}
									value={confirmPassword}
									onChange={(e) => setConfirmPassword(e.target.value)}
								/>
								<Form.Text className="text-muted">
									Should match password above.
								</Form.Text>
							</Form.Group>
							
							<Button type="submit" variant="warning" disabled={isDisabled}>
								{ isLoading
									? <span>
											<span 
												className="spinner-border spinner-border-sm"
												role="status"
												aria-hidden="true">
											</span>
											<span> Editing...</span>
										</span>
									: 'Edit'
								}
							</Button>
						</Form>
				</Card.Body>
			</Card>
			<Link href="/categories">
				<Button className="btn-info d-block w-40 mt-2" disabled={isLoading}>Go Back</Button>
			</Link>
		</Fragment>
	)
}
